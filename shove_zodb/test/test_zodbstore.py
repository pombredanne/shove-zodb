# -*- coding: utf-8 -*-
'''shove ZODB store tests'''

from stuf.six import unittest

from shove.test.test_store import Store


class TestZODBStore(Store, unittest.TestCase):

    initstring = 'zodb://test.db'

    def tearDown(self):
        self.store.close()
        import os
        os.remove('test.db')
        os.remove('test.db.index')
        os.remove('test.db.tmp')
        os.remove('test.db.lock')